package com.example;

import com.amazonaws.services.sqs.model.Message;
import com.example.interfaces.IQueueService;
import com.example.interfaces.IQueueTimer;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TimerTask;
import java.util.UUID;

public class InMemoryQueueService implements IQueueService {

    private static final long VISIBILITY_TIMEOUT_DEFAULT = 10000L;
    private final Map<String, Deque<Message>> messageQueue;
    private final Map<String, TimerTask> pendingVisibilityTasks;
    private long visibilityTimeout;
    private final IQueueTimer queueTimer;

    public InMemoryQueueService(IQueueTimer queueTimer) {
        this(queueTimer, VISIBILITY_TIMEOUT_DEFAULT);
    }

    public InMemoryQueueService(IQueueTimer queueTimer, long visibilityTimeout) {
        this.messageQueue = new HashMap<>();
        this.pendingVisibilityTasks = new HashMap<>();
        this.visibilityTimeout = visibilityTimeout;
        this.queueTimer = queueTimer;
    }

    @Override
    public synchronized void push(String queueName, Message message) throws InterruptedException {
        if (messageQueue.containsKey(queueName)) {
            messageQueue.get(queueName).push(message);
        } else {
            Deque<Message> queue = new LinkedList<>();
            queue.push(message);
            messageQueue.put(queueName, queue);
        }
    }

    @Override
    public synchronized Message pull(String queueName) throws InterruptedException {
        if (messageQueue.containsKey(queueName) && messageQueue.get(queueName) != null) {
            Deque<Message> queue = messageQueue.get(queueName);
            Message head = queue.pollFirst();
            if (head == null) {
                return null;
            }
            String receiptHandle = UUID.randomUUID().toString();
            head.setReceiptHandle(receiptHandle);
            queueTimer.execute(receiptHandle, visibilityTimeout, head, queue, pendingVisibilityTasks);
            return head;
        }
        return null;
    }

    @Override
    public synchronized void delete(String receiptHandle) throws InterruptedException {
        if (pendingVisibilityTasks.containsKey(receiptHandle)) {
            pendingVisibilityTasks.get(receiptHandle).cancel();
            pendingVisibilityTasks.remove(receiptHandle);
        }
    }

    public Deque<Message> getQueue(String queueName) {
        return messageQueue.containsKey(queueName) ? messageQueue.get(queueName) : null;
    }

}
