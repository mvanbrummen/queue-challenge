package com.example;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.Message;
import com.example.interfaces.IQueueService;

public class SqsQueueService implements IQueueService {
    //
    // Task 4: Optionally implement parts of me.
    //
    // This file is a placeholder for an AWS-backed implementation of QueueService.  It is included
    // primarily so you can quickly assess your choices for method signatures in QueueService in
    // terms of how well they map to the implementation intended for a production environment.
    //

    // * For this I would see how closely my method signatures for my queue interface aligns with the amazon sqs clients
    // * For each of the interface methods, write the equivalent AmazonSQSClient code

    public SqsQueueService(AmazonSQSClient sqsClient) {

    }

    @Override
    public void push(String queueName, Message message) throws InterruptedException {

    }

    @Override
    public Message pull(String queueName) throws InterruptedException {
        return null;
    }

    @Override
    public void delete(String receiptHandle) throws InterruptedException {

    }

}
