package com.example.interfaces;

import com.amazonaws.services.sqs.model.Message;

import java.util.Deque;
import java.util.Map;
import java.util.TimerTask;

public interface IQueueTimer {

    void execute(String receiptHandle, long visibilityTimeout, Message head, Deque<Message> queue,
                 Map<String, TimerTask> pendingVisibilityTasks);

}
