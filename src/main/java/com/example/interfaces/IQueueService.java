package com.example.interfaces;

import com.amazonaws.services.sqs.model.Message;

public interface IQueueService {

    void push(String queueName, Message message) throws InterruptedException;

    Message pull(String queueName) throws InterruptedException;

    void delete(String receiptHandle) throws InterruptedException;

}
