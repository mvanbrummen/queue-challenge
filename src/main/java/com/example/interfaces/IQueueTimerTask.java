package com.example.interfaces;

import com.amazonaws.services.sqs.model.Message;

import java.util.Deque;
import java.util.Map;
import java.util.TimerTask;

public interface IQueueTimerTask {

    void setQueue(Message head, Deque<Message> queue, Map<String, TimerTask> pendingVisibilityTasks);

}
