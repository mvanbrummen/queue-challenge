package com.example;

import com.amazonaws.services.sqs.model.Message;
import com.example.interfaces.IQueueTimer;
import com.example.interfaces.IQueueTimerTask;

import java.util.Deque;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class QueueTimer implements IQueueTimer {

    private final Timer timer = new Timer();

    public void execute(String receiptHandle, long visibilityTimeout, Message head, Deque<Message> queue,
                        Map<String, TimerTask> pendingVisibilityTasks) {
        IQueueTimerTask queueTimerTask = new QueueTimerTask();
        queueTimerTask.setQueue(head, queue, pendingVisibilityTasks);
        timer.schedule((TimerTask) queueTimerTask, visibilityTimeout);
        pendingVisibilityTasks.put(receiptHandle, (TimerTask) queueTimerTask);
    }

    private class QueueTimerTask extends TimerTask implements IQueueTimerTask {

        private Message head;
        private Deque<Message> queue;
        private Map<String, TimerTask> pendingVisibilityTasks;

        @Override
        public void run() {
            queue.add(head);
            pendingVisibilityTasks.remove(head.getReceiptHandle());
        }

        @Override
        public void setQueue(Message head, Deque<Message> queue, Map<String, TimerTask> pendingVisibilityTasks) {
            this.head = head;
            this.queue = queue;
            this.pendingVisibilityTasks = pendingVisibilityTasks;
        }

    }

}
