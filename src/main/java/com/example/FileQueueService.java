package com.example;

import com.amazonaws.services.sqs.model.Message;
import com.example.interfaces.IQueueService;

public class FileQueueService implements IQueueService {
    //
    // Task 3: Implement me if you have time.
    //

    // * I would create the lock and unlock methods like on the Canva hermeticity blog post for thread safety around critical sections/write operations
    @Override
    public void push(String queueName, Message message) throws InterruptedException {
        // * implement similar to the blog without the delay and keep it to a single message for simplicity
        // * each queue gets a new dir
        // * append new messages to existing messages files
    }

    @Override
    public Message pull(String queueName) throws InterruptedException {
        // * recreate the messages file with the particular message removed
        // * have a TimerTask read all the current messages and recreate with the message reinstated after the visibility timeout
        // * cancel the TimerTask if the message hadnt been deleted
        return null;
    }

    @Override
    public void delete(String receiptHandle) throws InterruptedException {
        // * have a Map<String, TimerTask> match recept handle to a TimerTask
        // * if it exists, cancel the task to stop the message being appended to the queue file
    }


}
