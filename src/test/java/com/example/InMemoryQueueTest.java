package com.example;

import com.amazonaws.services.sqs.model.Message;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

public class InMemoryQueueTest {

    private InMemoryQueueService queueService;
    private CountDownLatch latch;

    @Before
    public void setup() {
        latch = new CountDownLatch(1);
        queueService = new InMemoryQueueService(new TestQueueTimer(latch), 1);
    }

    @Test
    public void whenPushThenMessageAdded() throws InterruptedException {
        queueService.push("q1", createMessage("1"));
        queueService.push("q1", createMessage("2"));
        queueService.push("q1", createMessage("3"));
        Assert.assertEquals(3, queueService.getQueue("q1").size());
        queueService.push("q2", createMessage("1"));
        Assert.assertEquals(1, queueService.getQueue("q2").size());
        queueService.push("q3", createMessage("1"));
        queueService.push("q3", createMessage("2"));
        Assert.assertEquals(2, queueService.getQueue("q3").size());
    }

    @Test
    public void whenPullThenMessageReturned() throws InterruptedException {
        Message msg = createMessage("1");
        queueService.push("q1", msg);
        Assert.assertEquals(msg, queueService.pull("q1"));
    }

    @Test
    public void whenPullEmptyQueueThenNullReturned() throws InterruptedException {
        Assert.assertNull(queueService.pull("q1"));
    }

    @Test
    public void whenPullThenMessageNotVisibleImmediately() throws InterruptedException {
        queueService.push("q1", createMessage("1"));
        queueService.push("q1", createMessage("2"));
        queueService.push("q1", createMessage("3"));
        queueService.pull("q1");
        Assert.assertEquals(2, queueService.getQueue("q1").size());
    }

    @Test
    public void whenPullAndTimeoutLapseThenMessageReturnedToQueue() throws InterruptedException {
        queueService.push("q1", createMessage("1"));
        queueService.push("q1", createMessage("2"));
        queueService.push("q1", createMessage("3"));
        queueService.pull("q1");
        latch.await();
        Assert.assertEquals(3, queueService.getQueue("q1").size());
    }

    @Test
    public void whenPullMultipleAndTimeoutLapseThenMessageReturnedToQueue() throws InterruptedException {
        queueService.push("q1", createMessage("1"));
        queueService.push("q1", createMessage("2"));
        queueService.push("q1", createMessage("3"));
        String handle = queueService.pull("q1").getReceiptHandle();
        latch.await();
        queueService.delete(handle);
        Assert.assertEquals(3, queueService.getQueue("q1").size());
    }

    @Test
    public void whenDeleteThenMessageRemoved() throws InterruptedException {
        queueService.push("q1", createMessage("1"));
        queueService.push("q1", createMessage("2"));
        queueService.push("q1", createMessage("3"));
        Message msg = queueService.pull("q1");
        queueService.delete(msg.getReceiptHandle());
        Assert.assertEquals(2, queueService.getQueue("q1").size());
    }

    @Test
    public void whenDeleteMultipleThenMessageRemoved() throws InterruptedException {
        queueService.push("q1", createMessage("1"));
        queueService.push("q1", createMessage("2"));
        queueService.push("q1", createMessage("3"));
        queueService.delete(queueService.pull("q1").getReceiptHandle());
        queueService.delete(queueService.pull("q1").getReceiptHandle());
        Assert.assertEquals(1, queueService.getQueue("q1").size());
    }

    @Test
    public void whenDeleteNonExistentReceiptHandleThenMessageNotDeleted() throws InterruptedException {
        queueService.push("q1", createMessage("1"));
        queueService.delete("boo-urns");
        Assert.assertEquals(1, queueService.getQueue("q1").size());
    }

    @Test
    public void whenDeleteAfterTimeoutThenMessageNotRemoved() throws InterruptedException {
        queueService.push("q1", createMessage("1"));
        queueService.push("q1", createMessage("2"));
        queueService.push("q1", createMessage("3"));
        Message msg = queueService.pull("q1");
        latch.await();
        queueService.delete(msg.getReceiptHandle());
        Assert.assertEquals(3, queueService.getQueue("q1").size());
    }

    @Test
    public void whenGetQueueThenReturnQueue() throws InterruptedException {
        queueService.push("q1", createMessage("1"));
        queueService.push("q1", createMessage("2"));
        queueService.push("q2", createMessage("3"));
        queueService.push("q2", createMessage("4"));
        queueService.push("q2", createMessage("5"));
        Assert.assertEquals(3, queueService.getQueue("q2").size());
    }

    @Test
    public void whenGetNonExistentQueueThenReturnNull() throws InterruptedException {
        Assert.assertNull(queueService.getQueue("q2"));
        queueService.push("q1", createMessage("1"));
        Assert.assertNull(queueService.getQueue("q2"));
    }

    private Message createMessage(String message) {
        Message msg = new Message();
        msg.setBody(message);
        return msg;
    }

}
